// Daanyal Farooqi and Syed Ateeb Jamal
package app;

public class Song implements Comparable {
    String name;
    String artist;
    String album;
    int year;

    public Song(String name, String artist, String album, int year) {
        this.name = name;
        this.artist = artist;
        this.album = album;
        this.year = year;
    }

    /*public Song(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.album = null;
        this.year = 0;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public int compareTo(Object o) {
        return compareTo((Song) o);
    }

    public int compareTo(Song s){
        if(this.name.toLowerCase().equals(s.name.toLowerCase())){
            return this.artist.toLowerCase().compareTo(s.artist.toLowerCase());
        }
        else{
            return this.name.toLowerCase().compareTo(s.name.toLowerCase());
        }
    }

    @Override
    public String toString(){
        return name + " - " + artist;
    }

    public String displayString(){
        if(year == 0){
            return "Song: " + name + "\n" +
                    "Artist: " + artist + "\n" +
                    "Album: " + album + "\n" +
                    "Year: ";
        }
        else {
            return "Song: " + name + "\n" +
                    "Artist: " + artist + "\n" +
                    "Album: " + album + "\n" +
                    "Year: " + year;
        }
    }

    public String csvString(){
        return name + "," + artist + "," + album + "," + year + "\n";
    }
}
