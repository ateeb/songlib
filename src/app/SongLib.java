// Daanyal Farooqi and Syed Ateeb Jamal
package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SongLib extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        AddController.indexAdded = -1;

        Parent root = FXMLLoader.load(getClass().getResource("layout.fxml"));

        primaryStage.setTitle("Song Library");

        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
