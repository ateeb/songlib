// Daanyal Farooqi and Syed Ateeb Jamal
package app;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class AddController implements Initializable {

    @FXML TextField name;
    @FXML TextField artist;
    @FXML TextField album;
    @FXML TextField year;
    @FXML Button submit;
    @FXML Button cancel;

    public static int indexAdded = -1;

    public void initialize(URL url, ResourceBundle rb){

        EventHandler<ActionEvent> submitAdd = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e){
                String songIn = name.getText();
                String artistIn = artist.getText();
                String albumIn = album.getText();
                int yearIn;

                if(!year.getText().equals("")) {
                    try {
                        yearIn = Integer.parseInt(year.getText());
                    } catch (Exception ex) {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.initOwner(submit.getScene().getWindow());
                        alert.setHeaderText("Year must be a valid number!");
                        alert.showAndWait();
                        return;
                    }
                }
                else{
                    yearIn = 0;
                }

                if(songIn.equals("") || artistIn.equals("")){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.initOwner(submit.getScene().getWindow());
                    alert.setHeaderText("Song name and artist are required!");
                    alert.showAndWait();
                    return;
                }

                Song s = new Song(songIn, artistIn, albumIn, yearIn);

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Add\n" + s.displayString(), ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.YES) {
                    boolean exists = false;

                    for(Song a : Controller.songs){
                        if(a.name.toLowerCase().equals(s.name.toLowerCase()) && a.artist.toLowerCase().equals(s.artist.toLowerCase())){
                            exists = true;
                            break;
                        }
                    }

                    if(exists){
                        Alert alertN = new Alert(Alert.AlertType.WARNING, "Song with these details exists.\nPress Ok to edit and cancel to quit.", ButtonType.OK, ButtonType.CANCEL);
                        alertN.showAndWait();

                        if (alertN.getResult() == ButtonType.OK){
                            return;
                        }
                        else{
                            Controller.songs = null;
                            Controller.ol = null;
                        }
                    }
                    else{
                    Controller.songs.add(s);
                    Collections.sort(Controller.songs);
                    Controller.ol.clear();
                    for (Song x : Controller.songs) {
                        Controller.ol.add(x.toString());
                    }

                    indexAdded = Controller.songs.indexOf(s);
                    }
                }
                else if(alert.getResult() == ButtonType.CANCEL){
                    Controller.songs = null;
                    Controller.ol = null;
                }
                else{
                    return;
                }

                Stage window = (Stage) submit.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("layout.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };

        submit.setOnAction(submitAdd);

        EventHandler<ActionEvent> quit = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Controller.songs = null;
                Controller.ol = null;

                Stage window = (Stage) submit.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("layout.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };

        cancel.setOnAction(quit);
    }

}
