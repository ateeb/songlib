// Daanyal Farooqi and Syed Ateeb Jamal
package app;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    ListView<String> listView;
    static ArrayList<Song> songs;
    static ObservableList<String> ol;
    static Song selectedSong;

    @FXML ListView<String> lv;
    @FXML Label display;
    @FXML Button delete;
    @FXML Button add;
    @FXML Button edit;

    //TextInputDialog addDialog = new TextInputDialog();

    public void initialize(URL url, ResourceBundle rb) {
        //writeOriginal();

        //Load in Data
        if(songs == null || songs.isEmpty()) {
            songs = new ArrayList<Song>();
            read();
            Collections.sort(songs);
        }


        if ((ol == null || ol.isEmpty()) && (songs == null || songs.isEmpty())){
            display.setText("You don't have any songs");
        }
        //Set Up List View
            if ((ol == null || ol.isEmpty()) && !(songs == null || songs.isEmpty())) {
            ol = FXCollections.observableArrayList();
            for (Song s : songs) {
                ol.add(s.toString());
            }
            lv.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            lv.setItems(ol);
            lv.getSelectionModel().select(0);
            display.setText(songs.get(0).displayString());
        }
        else if (!(songs == null || songs.isEmpty())) {
            write();
            lv.setItems(ol);

            if (AddController.indexAdded != -1) {
                lv.getSelectionModel().select(AddController.indexAdded);
                display.setText(songs.get(AddController.indexAdded).displayString());
            }
            else {
                lv.getSelectionModel().select(EditController.indexAdded);
                display.setText(songs.get(EditController.indexAdded).displayString());
            }
        }
        else{
                ol = FXCollections.observableArrayList();
            }

        if(!(songs == null || songs.isEmpty()))
    {
        selectedSong = songs.get(lv.getSelectionModel().getSelectedIndex());}
        //Set display on click
        lv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                for (Song s : songs) {
                    if (s.toString().equals(newValue)) {
                        selectedSong = s;
                        display.setText(s.displayString());
                    }
                }
            }
        });


        //Delete a Song
        EventHandler<ActionEvent> removeSong = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                int newSelect = lv.getSelectionModel().getSelectedIndex();

                for (int i = 0; i < songs.size(); i++) {
                    if (songs.get(i).toString().equals(lv.getSelectionModel().getSelectedItem())) {

                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Delete\n" + songs.get(i).displayString(), ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
                        alert.showAndWait();

                        if (alert.getResult() == ButtonType.YES) {
                            songs.remove(songs.get(i));
                            ol.remove(lv.getSelectionModel().getSelectedItem());
                            lv.setItems(ol);
                            lv.getSelectionModel().select(newSelect);
                            if (ol.size() == 0) {
                                display.setText("No song selected");
                            }
                            write();
                        }
                        break;
                    }
                }
            }
        };

        delete.setOnAction(removeSong);

        //Add a song
        EventHandler<ActionEvent> addView = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) add.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("add.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        };

        add.setOnAction(addView);

        //Edit a song
        EventHandler<ActionEvent> editView = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) edit.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("edit.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        };

        edit.setOnAction(editView);
    }

    public static void read(){

        try (BufferedReader br = new BufferedReader(new FileReader("songs.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                songs.add(new Song(data[0], data[1], data[2], Integer.parseInt(data[3])));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void write(){
        String data = "";

        for(Song s : songs){
            data += s.csvString();
        }

        try {

            PrintWriter out = new PrintWriter("songs.txt");
            out.print(data);
            out.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void writeOriginal(){
        Song a = new Song("Baby", "Justin Beiber", "My Environment", 2009);
        Song b = new Song("Not Afraid", "Eminem", "Road to Recovery", 2010);
        Song c = new Song("Baby", "Alex Jones", "Some Album", 3003);
        Song d = new Song("My Jam", "Ateeb Jam", "", 0);
        songs = new ArrayList<Song>();
        songs.add(a);
        songs.add(b);
        songs.add(c);
        songs.add(d);

        write();
    }
}
